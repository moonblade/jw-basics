# Basics

The aim of this repo is to get through the basics of a lot of auxialliary tech so that all of it can be used effectively for your day to day work.

## [Git](git/README.md)

## [Bash](bash/README.md)

## [Docker](docker/README.md)

## [Kubernetes](k8s/README.md)

# Ideas

1. APIs
2. Python - some basics of programming and libraries
3. Helm
4. GitOps
5. React

